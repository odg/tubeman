#!/bin/sh
# This scipt generates a .deb package for TubeMan
# It should work on Debian, Ubuntu or any other Debian-based system

#Do this to get the build dependencies:
# sudo apt-get install build-essential libsqlite3-dev libcurl-dev libncursesw5-dev
#Do this before running this script to grab the packaging dependencies:
# sudo apt-get install debhelper devscripts gnupg lintian

if ! test -f configure.ac || ! test -d src; then
	echo "Error: run this script from root of the repository!"
	exit 1
elif ! dpkg --version >/dev/null 2>&1; then
	echo "Error: this script should be run on a Debian-based system!"
	exit 1
fi

#Set some variables
#shellcheck disable=SC2016
{
name=$(autoconf -t 'AC_INIT:$1')
devmail=$(autoconf -t 'AC_INIT:$3')
url=$(autoconf -t 'AC_INIT:$5')
}
devname="Oliver Galvin"
dir="debian"

#Change these to the details of the package maintainer if necessary
export DEBEMAIL="$devmail"
export DEBFULLNAME="$devname"

{ ./configure; make dist; } > /dev/null
rm -rf "$dir"
mkdir -p "$dir/source"
cp ChangeLog "$dir/changelog"
cp NEWS TODO README "$dir"
cd "$dir" || exit 1
cat <<EOF > control
Source: $(echo "$name" | tr '[:upper:]' '[:lower:]')
Section: web
Priority: optional
Maintainer: ${DEBFULLNAME} <${DEBEMAIL}>
Build-Depends: libsqlite3-dev, libcurl4-openssl-dev, libncursesw5-dev, gettext, debhelper, autotools-dev
Standards-Version: 3.9.8
Homepage: $url

Package: $(echo "$name" | tr '[:upper:]' '[:lower:]')
Architecture: any
Depends: \${shlibs:Depends}, \${misc:Depends}
Description: Command line YouTube subscription manager and browser
 YouTube subscription manager and browser, with NewPipe compatible database.
 View and download videos from the command line, keep track of channels you
 subscribe to locally and privately, and with free software. Includes optional
 ncurses-based interface, with search function and thumbnail viewer.
 .
 Lightweight, written in portable C, with minimal dependencies.
EOF
cat <<EOF > copyright
Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Upstream-Name: $name
Upstream-Contact: $devname <$devmail>
Source: $url

Files: *
Copyright: 2017-$(date +%Y) $devname <$devmail>
License: GPL-3+

License: GPL-3+
 $name is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 $name is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with $name.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 '/usr/share/common-licenses/GPL-3'.
EOF
cat <<EOF > rules
#!/usr/bin/make -f
%:
	dh \$@ --with autotools_dev
EOF
chmod +x rules
echo 9 > compat
echo "3.0 (native)" > source/format
echo "docs/$name.1" > "$name.manpages"
cd ..
debuild -i --no-sign -us -ui -uc
