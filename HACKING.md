# TubeMan

## Hacking

Options and notes for TubeMan aimed at developers/testers/packagers only.  
See `README.md` for a more general description and installation notes, 
or the man page for usage.

### Bootstrapping the repository

Bootstrapping is only necessary from git, 
never when building from release archives.

To set up the source directory and build straight from git, you need:

* automake
* autoconf
* gettext (if you want translation support)

Once you've cloned the git repo, in environments that support configure shell 
scripts, to bootstrap and build run
```
./autogen.sh
./configure
make
```
with whatever configure options you want.

The standard make targets should work, see 
[here](https://gnu.org/software/make/manual/html_node/Standard-Targets.html).  
`make distclean` should delete all the stuff created by configure.

### configure options

**--enable-debug**  
Enables assertions  
Debug mode is disabled by default  
I use `./configure --enable-debug CFLAGS='-g'` for debugging

**--disable-tui**  
Compiles without the curses-based TUI  
TUI is enabled by default

**--disable-nls**  
Compiles without internationalisation (i18n) support  
i18n is enabled by default

### Testing

The `/testing` directory contains scripts and suppression files for 
uncrustify, cppcheck and valgrind.
