/*
  Copyright (c) 2018-2019 Oliver Galvin <odg at riseup dot net>
  This file is part of TubeMan.
  A local YouTube subscription manager and video browser.

 TubeMan is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TubeMan is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TubeMan.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef NDEBUG
# define _FORTIFY_SOURCE 2		/*buffer overflow checks */
#endif

#include <stdlib.h>
#include <gtk/gtk.h>
/*#include "tubeman.h"*/

static void setupmainwindow(GtkApplication* app, gpointer user_data)
{
	GtkWidget *window = gtk_application_window_new (app);
	gtk_window_set_title(GTK_WINDOW(window), PACKAGE_NAME);
	gtk_window_set_default_size(GTK_WINDOW(window), 800, 500);
	gtk_widget_show_all(window);
}

static void showreadmewindow(GtkBuilder *builder)
{
	GtkWidget *readmewindow;
	GtkTextView *readmebox;
	GtkTextBuffer *buff = gtk_text_buffer_new(NULL);
	gchar *str = NULL;
	readmewindow = GTK_WIDGET(gtk_builder_get_object(builder, "readmewindow"));
	readmebox = GTK_TEXT_VIEW(gtk_builder_get_object(builder, "readmebox"));
	if(!gtk_text_view_get_buffer(readmebox)) {
		if(!g_file_get_contents(DOCDIR"/README", &str, NULL, NULL)) {
			str = g_strdup("Could not show the README file!");
		}
		gtk_text_buffer_set_text(buff, str, -1);
		gtk_text_view_set_buffer(readmebox, buff);
		g_free(str);
	}
	gtk_widget_show_all(readmewindow);
}

static void showaboutwindow()
{
	const char *description = " \
GTK frontend for TubeMan, a YouTube subscription manager and browser, with \
NewPipe compatible database. View and download videos, keep track of channels \
you subscribe to locally, privately, and with free software.\n \
\n \
Lightweight, written in portable C with minimal dependencies.";
	gtk_show_about_dialog(NULL,
	                      "program-name", PACKAGE_NAME,
	                      "title", "About "PACKAGE_STRING,
	                      "license-type", GTK_LICENSE_GPL_3_0,
	                      "copyright", "© 2017-2018 Oliver Galvin",
	                      "comments", description,
	                      "version", PACKAGE_VERSION,
	                      "website", PACKAGE_URL,
	                      "website-label", "Source Repository",
	                      NULL);
}

int main(int argc, char **argv)
{
	GtkBuilder *builder;
	GObject *aboutopt, *readmeopt, *quitopt;
	GObject *mainwindow, *aboutwindow;

	gtk_init(&argc, &argv);
	if(!(builder = gtk_builder_new_from_file(PKGDATADIR"/tubeman.glade"))) {
		g_printerr("Could not find XML UI definition file\n");
		return EXIT_FAILURE;
	}
	gtk_window_set_default_icon_from_file(PKGDATADIR"/icon.png", NULL);

	mainwindow = gtk_builder_get_object(builder, "mainwindow");
	g_signal_connect(mainwindow, "destroy", G_CALLBACK(gtk_main_quit), NULL);

	aboutwindow = gtk_builder_get_object(builder, "aboutwindow");
	aboutopt = gtk_builder_get_object(builder, "showabout");
	g_signal_connect(aboutopt, "activate", G_CALLBACK(showaboutwindow), NULL);

	readmeopt = gtk_builder_get_object(builder, "showreadme");
	g_signal_connect_swapped(readmeopt, "activate", G_CALLBACK(showreadmewindow), builder);

	quitopt = gtk_builder_get_object(builder, "quit");
	g_signal_connect(quitopt, "activate", G_CALLBACK(gtk_main_quit), NULL);

	gtk_widget_show_all(GTK_WIDGET(mainwindow));
	/*GtkApplication *app;
	app = gtk_application_new("tf.odg.tubeman", G_APPLICATION_FLAGS_NONE);
	g_signal_connect(app, "activate", G_CALLBACK(setupmainwindow), NULL);
	err = g_application_run(G_APPLICATION(app), argc, argv);
	g_object_unref(app);*/
	gtk_main();
	return EXIT_SUCCESS;
}
