/*
  Copyright (c) 2018-2019 Oliver Galvin <odg at riseup dot net>
  This file is part of TubeMan.
  A local YouTube subscription manager and video browser.

 TubeMan is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TubeMan is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TubeMan.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __TUBEMAN_H__
#define __TUBEMAN_H__

/*Enable including in C++ code*/
#ifdef __cplusplus
extern "C" {
#endif

/* Records in subscription table, matching Newpipe's layout         */
typedef struct achannel {
	long   id;        /* Unique ID                              */
	int    x;
	char  *url;       /* Channel URL, youtube.com/channel/cid   */
	char  *name;      /* Channel name                           */
	char  *thumb;     /* URL of the channel's thumbnail         */
	long   subs;      /* Number of youtube.com subscriptions    */
	char  *desc;      /* Channel's description                  */
} achannel;

/* Records in video tables, one table for each subscription         */
typedef struct avideo {
	int    id;        /* ID, oldest in a channel is 0           */
	char  *vid;       /* Hash ID for the video                  */
	char  *title;     /* Title of video                         */
	long   duration;  /* Duration, in seconds                   */
	time_t timestamp; /* Unix timestamp                         */
	char  *desc;      /* Video's description                    */
	int    viewed;    /* Has it been marked as viewed?          */
	char  *licence;   /* The licence of the video               */
} avideo;

/* Set of configuration options                                     */
typedef struct aconf {
	char  *player;    /* Player to use when playing streams     */
	char  *format;    /* Preferred format: webm, mp4 or 3gp     */
	long   res;       /* Preferred vertical resolution          */
	long   proxy;     /* Optional proxy type                    */
	char  *ip;        /* Optional proxy IP address              */
	long   port;      /* Optional proxy port                    */
	long   list;      /* Default number of items to show in list*/
	char  *sort;      /* Default sorting: oldest or newest first*/
	int    colour;    /* Show colour in terminal output?        */
	char  *subs;      /* Custom subscription database location  */
	char  *vids;      /* Custom video database location         */
	char  *thumbs;    /* Custom thumbnail cache location        */
	int    tshow;     /* Show thumbnails in TUI mode?           */
	int    tsave;     /* Keep thumbnails after closing?         */
	int    vim;       /* Use vim-style keybindings?             */
	int    enc;       /* Encrypt databases with SQLCipher?      */
	int    verbose;   /* Verbose mode?                          */
} aconf;

/*
 These are functions exposed to be used by TubeMan frontends.
 To use TubeMan you should not need to link libsqlite or libcurl,
 just use the functions below to modify the databases or make queries.
 Functions return 0 on success, 1 on failure, unless stated otherwise.
 */

/*Subscribe to a channel, get necessary info and create video table */
int addchannel(const char *cid, const char *name);

/*Remove channel from subscriptions, and its associated video table */
int delchannel(const char *term);

/*Search subscription table, returning array of channels in output  */
int searchchannels(const char *term, achannel **output);

/*Merge given SQLite/OPML database into current subscriptions       */
int importdb(const char *filename);

/*Get raw video URL(s) for a given Youtube video with VID vid       */
/* Returns 2 element array of URLs                                  */
char** getyoutubestream(const char *vid);

/*Check if we have a thumbnail, and if not, download it             */
/* Returns the image filename if it exists, NULL otherwise          */
char* getimage(const char *id, const char *thumburl);

/*Update video listing for every subscription                       */
int syncvids(void);

/*Download video at given Youtube URL                               */
/*If dest is not NULL, saves there, otherwise uses current directory*/
int downloadvideo(const char *url, const char *dest);

#ifdef __cplusplus
}
#endif

#endif /*__TUBEMAN_H__*/
