% tubeman(1) | TubeMan Manual

# NAME

tubeman - A unix YouTube subscription manager and browser

# SYNOPSIS

**tubeman** \[**-S**\] \[**-Q**\] \[**-F**\] \[**-p**\] \[**-i**\] \[**\--gen-config**\] \[**-h**\] \[**-v**\]  

# DESCRIPTION

TubeMan is a small command line program for managing subscriptions to YouTube channels, browsing, and viewing videos. All subscriptions and video data are stored 
locally for your privacy. TubeMan aims to be portable, efficient and simple. Its database is also comatible with the Android app NewPipe.

If you have used Pacman, the Arch Linux package manager, the command line interface should be familiar. Using the Pacman metaphor, channels are treated like packages, 
and videos are treated like files.

If you are not used to Pacman, simply running TubeMan with no options starts interactive mode, with an easy to use GUI-like interface and optional vim keybindings.

# OPTIONS

-S, \--sync \[options\]
:  Add subscription(s)

-R, \--remove \[options\]
:  Remove given subscription(s)

-Q, \--query
:  Channel queries

-F, \--file
:  Video queries

-p, \--play [URL]
:  Play a video at the given URL

-i, \--import [option]
:  Import a TubeMan/NewPipe database, or an OPML file from Youtube

\--gen-config
:  Copy default configuration file to a default config directory

\--config [file]
:  Override the default behaviour to force TubeMan to use the configuration file at the location given

-h, \--help
:  Display usage information for a given option

-v, \--version
:  Display version information

# SYNC OPTIONS

-Sy
:  Update details for all subscriptions

-Syu
:  Update details and video listings for all subscriptions

-Si [option]
:  Show information about given channel on Youtube

-Ss [option]
:  Search for a channel on Youtube

# QUERY OPTIONS

-Qs  
:  List all subscribed channels

-Qs [option]
:  Search for a channel in your subscriptions

-Qi [option]
:  Show locally saved information about a given channel

# FILE OPTIONS

-Fs [option]
:  Search for videos

-Fl [option]
:  List videos of given channel

-Fi [option]
:  Show video info

-Fd [URL]
:  Download video

# FILES

By default, TubeMan looks for a configuration file in these locations, in this order:

* $XDG_CONFIG_HOME/tubeman/config
* ~/.config/tubeman/config
* ~/.tubeman

The **\--gen-config** option copies the default config file to the first of these locations it can access.  
The default config can be found at `/usr/share/doc/tubeman/config`.

If a database file is not set in the config file, TubeMan looks for it in $XDG_DATA_HOME/tubeman.  
If XDG_DATA_HOME is not set, it looks in ~/.local/share/tubeman.  
If no database is found, an empty one is created when TubeMan is run.

# BUGS

To report a bug, submit an issue at <https://notabug.org/odg/tubeman/issues>, or email the author.

# COPYRIGHT

Copyright Oliver Galvin <odg@riseup.net>, 2017-2018.  
Licensed under GNU GPLv3 or later.

# SEE ALSO

See the README, usually at `/usr/share/doc/tubeman/README` or at <https://notabug.org/odg/tubeman>, for more information.
