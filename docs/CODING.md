# TubeMan

## Notes relevent to developers only
 
Long description is used in man page, debian package and about window
Short description is used in man page name, help text

### Testing

 * Testing scripts in /testing
 * When compiling, warning messages are turned up to max
 * CC=testing/gcc-strict can be used for even more warnings
 * Aim to be able to build with no warnings from the compiler or cppcheck

### Style

A brief set of style guidelines - to be enforced with uncrustify

 * K&R style
  * Braces start on the same line for if/else/for/do/while blocks
  * Function braces start on the line after the header on the same indent level
  * Ending braces on a new line
 * Use braces around every if/else/for/do/while block, even single statements
 * Use tabs to indent, spaces to align columns
 * Maximum 80 characters per line
 * No goto statements or multiple statements on one line
 * No global variables, and keep variables to least possible scope

### Portability

 * Use standard ISO C89 for tubeman, C99 for tubeman-gtk
 * Keep any platform specific code to a minimum
 * POSIX-specific code should have a WIN32 alternative
 * No GNU extensions, for macOS/BSD compatibility

### Internationalisation:

Using gettext, enclose any strings in _(), as suggested in gettext docs
