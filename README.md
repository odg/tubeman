```
              _______    _          __  __
             |__   __|  | |        |  \/  |
                | |_   _| |__   ___| \  / | __ _ _ __
                | | | | | '_ \ / _ \ |\/| |/ _' | '_ \
                | | |_| | |_) |  __/ |  | | (_| | | | |
                |_|\__,_|_.__/ \___|_|  |_|\__,_|_| |_|
```

**Please note TubeMan is not yet complete and so is unusable currently.  
This README describes both planned and implemented features.**

![TubeMan Icon](gtk/icon.png)

# Summary

A lightweight YouTube subscription manager and browser, with a 
[NewPipe](https://newpipe.schabi.org) compatible database and 
[pacman](https://archlinux.org/pacman) style command line interface. Follow 
channels and watch videos without needing the YouTube API, a Google account, 
or a bloated [nonfree](https://gnu.org/philosophy/javascript-trap) website. 
Stores your data locally, is configurable, and includes optional text based 
and graphical frontends.

## Goals

* Enable the user to browse and subscribe to channels using only free software,
  without sacrificing privacy
* Have a pacman-style command line interface, and an optional GUI
* To be small, efficient, written in portable C, and use minimal dependencies
* Do not require a Google account, allowing users to safely store data locally
* Use a database compatible with NewPipe
* Obey XDG directory guidelines

## Description

TubeMan is a Youtube subscription manager and video browser written in 
portable C code, so should run on virtually any system. Partially inspired by 
NewPipe, a free Android program allowing you to view videos and subscribe to 
channels locally, TubeMan aims to have a compatible database and use similar 
concepts in a clean terminal based interface.

By storing a subscription database locally (which can be backed up however you 
wish), you avoid needing a Google account, for increased privacy and not 
having to trust Google with your data and letting them build a profile of your 
interests. It also avoids the need for using their website and any proprietary 
Javascript or Flash, instead using a familiar and simpler native command line 
or graphical interface, free formats like WebM, VP9 video and Ogg audio, and 
free software players like mpv or VLC.

TubeMan looks for the configuration file in the following places, 
from first to last:

* $XDG_CONFIG_HOME/tubeman/config
* ~/.config/tubeman/config
* /etc/tubeman/config

The database location is set in the configuration or it is 
`$XDG_DATA_HOME/tubeman/subscriptions.db` by default.  
When run, TubeMan will generate a subscription database if one is not found.  
Optionally, you can import your existing database from an Android device 
running NewPipe.  

See `tubeman --help` or `man tubeman` for usage information.  
See the default config file for descriptions of the config options.  
Running TubeMan with no options starts interactive mode.

You can import your supscriptions from youtube.com by downloading an OPML file 
from Youtube by visiting 
<https://youtube.com/subscription_manager?action_takeout=1> 
while signed in, then import it with the `--import` option. You can add 
channels individually, the OPML file is an **optional** way to add your 
subscriptions. See the man page for more details.  

### Displaying Thumbnails

Displaying thumbnails in the terminal is an optional feature in the ncurses 
interface. The following methods are supported:

* w3m    - this works only with: urxvt, xterm and st
* tycat  - this works only in terminology
* iterm2 - this works only in iTerm2 on macOS

# Dependencies

* [SQLite](https://www.sqlite.org)
* [libcurl](https://curl.haxx.se/libcurl)
* [GNU ncurses](https://gnu.org/software/ncurses)

## Optional

* [w3m](http://w3m.sourceforge.net)

## Build dependencies

* [GNU gettext/libintl](https://gnu.org/software/gettext)

# Building

As TubeMan is in its early stages there are no prebuilt binaries yet, so you 
need to compile from source. Other than the dependencies, you should only need 
standard tools like a C compiler and make to build.

On Arch based GNU/Linux distros use the PKGBUILD 
[in the AUR](https://aur.archlinux.org), 
on macOS use [Homebrew](https://brew.sh).  
Otherwise, use one of the following commands to get what you need:  

    apt-get install build-essential gettext libsqlite3-0 libncurses5 libcurl3

    dnf install gcc make gettext sqlite3 ncurses-libs curl

    brew install gcc make gettext sqlite ncurses curl

Then get the code by cloning this git repo, then build and install by running:

    ./bootstrap
    ./configure
    make
    sudo make install

**HACKING.md** has more information on building and configure options.

# Credits

## Libraries

These are the libraries that TubeMan depends on.  

| Project                | Purpose              | License       |
| -----------------------| -------------------- | ------------- |
| SQLite                 | Managing databases   | Public Domain |
| libcurl                | Downloading data     | X11           |
| GNU ncurses            | Interactive mode     | X11           |
| GNU gettext            | Internationalisation | GPLv2+        |
| w3m                    | Displaying images    | Expat         |

### Included

These are small libraries included with TubeMan. They may have small changes 
but are written by their respective authors.  
See the **AUTHORS** file.

* [jsmn](https://github.com/zserge/jsmn),
  for tokenising JSON data  - Expat
* [inih](https://github.com/benhoyt/inih),
  for parsing .ini files    - Modified BSD
* [ya_getopt](https://github.com/kubo/ya_getopt),
  alternative option parser - FreeBSD

Test Tube icon by [Peter van Driel](https://thenounproject.com/petervandriel) 
from the Noun Project, released under the 
[CC BY](https://creativecommons.org/licenses/by/3.0/us) license.

## Prior Art

No code is used from these projects, but they inspired certain features.

* [NewPipe](https://github.com/TeamNewPipe/NewPipe) - a Youtube app for 
  Android that does not use the Youtube API and can store local subscriptions
* [ranger](https://ranger.github.io) - a terminal based file manager, for its 
  ncurses interface and image preview functionality
* [pacman](https://www.archlinux.org/pacman) -
  a package manager for Arch Linux, uses similar command line options
* [newsboat](https://github.com/newsboat/newsboat) -
  an active fork of newsbeuter, an ncurses-based feed reader

## Author

Copyright (C) Oliver Galvin, 2017-2019  
TubeMan is free software, released under the terms of the GNU 
[General Public License](https://www.gnu.org/copyleft/gpl.html), either 
version 3 of the License, or any later version. See the **COPYING** file for 
the full license text.

Email: **odg at riseup dot net**  
PGP: **491E 0D9E E7AA 9E15 D089 950A 7879 6625 7046 CC21**
